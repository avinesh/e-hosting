// add animate.css class(es) to the elements to be animated

$(document).ready(function () {
    /*------------------------------------
            post gallery slider nav icon
    --------------------------------------*/
    $('.carousel-control-prev-icon').append('<i class="icofont-thin-left"></i>');
    $('.carousel-control-next-icon').append('<i class="icofont-thin-right"></i>');





    $('.testimonial-slider').owlCarousel({
        loop: true,
        dots: true,
        nav:false,
        autoplay: false,
        margin:20,
        items: 1  ,
        responsive: {
            0: {
                mouseDrag: false,
                touchDrag:false,
                items: 1
            },
            600: {
                mouseDrag: false,
                touchDrag:false,
                items: 1

            },
            1000: {
                mouseDrag: true,
                touchDrag:true,
                items: 2

            }
        }
    });

    /*
     $('.banner-slider').owlCarousel({
         loop: true,
         dots: true,
         autoplay: false,
         smartSpeed:3000,
         nav: true,
         navText: ["<i class='icofont-thin-left'></i>", "<i class='icofont-thin-right'></i>"],
         items: 1  ,
         responsive: {
             0: {
                 nav: false,
                 mouseDrag: false,
                 touchDrag:false,
                 items: 1
             },
             600: {
                 nav: false,
                 mouseDrag: false,
                 touchDrag:false,
                 items: 2

             },
             1000: {
                 nav: true,
                 mouseDrag: true,
                 touchDrag:true,
                 items: 3

             }
         }
     });
 */



    /*------------------------------------
            back to top
    --------------------------------------*/
    $('.back-to-top').click(function () {      // When arrow is clicked
        $('body,html').animate({
            scrollTop: 0                       // Scroll to top of body
        }, 3000);
    });


    /*------------------------------------
            responsive menu
    --------------------------------------*/
    $('.main-menu-toggle').click(function () {
        $('.bspark_main_nav .main-menu-container-collapse').first().slideToggle('1000');
    });


    /*------------------------------------
        Sub Menu
      --------------------------------------*/
    $('nav .menu-item-has-children').append('<span class="sub-toggle"> <i class="fa fa-plus"></i> </span>');
    $('nav .page_item_has_children').append('<span class="sub-toggle-children"> <i class="fa fa-plus"></i> </span>');

    $('nav .sub-toggle').click(function () {
        $(this).parent('.menu-item-has-children').children('ul.sub-menu').slideToggle('1000');
        $(this).children('.fa-plus').first().toggleClass('fa-minus');
    });

    $('.navbar .sub-toggle-children').click(function () {
        $(this).parent('.page_item_has_children').children('ul.sub-menu').slideToggle('1000');
        $(this).children('.fa-plus').first().toggleClass('fa-minus');
    });


    /*------------------------------------
        Side menu open / close
      --------------------------------------*/
    $('.side-bar-toggler').on('click', function () {
        $('body').addClass('side-menu-open');
    });
    $('.close-side-menu').on('click', function () {
        $('body').removeClass('side-menu-open');
    });
    $('.side-overlay').on('click', function () {
        $('body').removeClass('side-menu-open');
    });

    $('.side-menu-nav .menu-item-has-children').append('<span class="sub-menu-dropdown-arrow"><i class="icofont-rounded-down"></i></span></li>');
    $('.side-menu-nav .menu-item-has-children').click(function () {
        $(this).children('ul.sub-menu').slideToggle('1000');
    });


    /*------------------------------------
        Side menu open / close
      --------------------------------------*/
    $('.search_main_menu ').click(function () {
        $('.search-content').addClass('search-content-act');
    });
    $('.search-close').click(function () {
        $('.search-content').removeClass('search-content-act');
    });


});
<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <link href="assets/library/font-awesome/css/fontawsome.css" type="text/css" rel="stylesheet"/>
    <link href="assets/library/icofont/icofont.min.css" rel="stylesheet" type="text/css">
    <link href="assets/library/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>

    <link href="assets/library/owlcarousel/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="assets/library/owlcarousel/css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
    <link href="style.css" type="text/css" rel="stylesheet"/>
</head>

<body>

<div id="page" class="site">
    <header class="transparent_header full_width">
        <div class="e_host_main_header ">
            <div class="container-fluid">
                <div class="row no-gutters">
                    <div class="logo">
                        <div class="site-branding">
                            <a href="#" class="custom-logo-link"><img width="290" height="50" src="assets/images/logo_dark.png" class="custom-logo" alt="Hitstoreqq" ></a>
                            <!--<h1 class="site-title">
                                <a href="#" rel="home">E Hosting Server</a>
                            </h1>
                            <p class="site-description">Just another WordPress site</p>-->
                        </div>
                    </div>
                    <div class="main-menu-wrapper">
                        <nav id="site-navigation" class="main-navigation">
                            <button class="main-menu-toggle" aria-controls="primary-menu" aria-expanded="false"><i
                                        class="fa fa-bars"></i></button>

                            <div class="main-menu-container-collapse">
                                <ul id="primary-menu" class="menu nav-menu right" aria-expanded="false">
                                    <li class="menu-item-has-children">
                                        <a href="index.php">Home</a>
                                        <ul class="sub-menu">
                                            <li><a href="index.php">Home Default</a></li>
                                            <li><a href="index-1.php">Home Layout One</a></li>
                                            <li><a href="index-2.php">Home Layout Two</a></li>
                                        </ul>
                                        <span class="sub-toggle"> <i class="fa fa-plus"></i> </span></li>
                                    <li class="menu-item-has-children"><a href="#">Pages</a>
                                        <ul class="sub-menu">
                                            <li><a href="#">Services</a>
                                                <ul class="sub-menu">
                                                    <li><a href="service.php">Service Layout One</a></li>
                                                    <li><a href="service-1.php">Service Layout Two</a></li>
                                                    <li><a href="service-2.php">Service Layout Three</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Team</a>
                                                <ul class="sub-menu">
                                                    <li><a href="team.php">Team Layout One</a></li>
                                                    <li><a href="team-1.php">Team Layout Two</a></li>

                                                </ul>
                                            </li>
                                            <li><a href="#">Faq</a>
                                                <ul class="sub-menu">
                                                    <li><a href="faq.php">Faq Layout One</a></li>
                                                    <li><a href="faq-1.php">Faq Layout Two</a></li>
                                                </ul>
                                            </li>

                                            <li><a href="#">Portfolio</a>
                                                <ul class="sub-menu">
                                                    <li><a href="portfolio-box.php">Portfolio Box</a></li>
                                                    <li><a href="portfolio-full.php">Portfolio Full</a></li>
                                                </ul>
                                            </li>

                                            <li><a href="#">Contact</a>
                                                <ul class="sub-menu">
                                                    <li><a href="contact.php">Contact Layout One</a></li>
                                                    <li><a href="contact-1.php">Contact Layout Two</a></li>
                                                </ul>
                                            </li>
                                        </ul>
                                        <span class="sub-toggle"> <i class="fa fa-plus"></i> </span></li>
                                    <li class="menu-item-has-children">
                                        <a href="#">Feature</a>
                                        <ul class="sub-menu">
                                            <li><a href="">Header</a>
                                                <ul class="sub-menu">
                                                    <li><a href="header-layout-1.php">Header Layout One</a></li>
                                                    <li><a href="header-layout-5.php">Header Layout Two</a></li>
                                                    <li><a href="header-layout-3.php">Header Layout Three</a></li>
                                                    <li><a href="header-layout-4.php">Header Layout Four</a></li>
                                                    <li><a href="header-layout-5.php">Header Layout Five</a></li>
                                                </ul>
                                            </li>

                                            <li><a href="">Footer</a>
                                                <ul class="sub-menu">
                                                    <li><a href="footer-layout-one.php">Footer Layout One</a></li>
                                                    <li><a href="footer-layout-two.php">Footer Layout Two</a></li>
                                                </ul>
                                            </li>

                                            <li><a href="">Page Title</a>
                                                <ul class="sub-menu">
                                                    <li><a href="page-title-one.php">Page Title One</a></li>
                                                    <li><a href="page-title-two.php">Page Title Two</a></li>
                                                    <li><a href="page-title-three.php">Page Title Three</a></li>
                                                    <li><a href="page-title-four.php">Page Title Four</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="call-to-action.php">Call To Action</a></li>
                                            <li><a href="">Promo Layout</a>
                                                <ul class="sub-menu">
                                                    <li><a href="promo-layout-one.php">Promo Layout One</a></li>
                                                    <li><a href="promo-layout-two.php">Promo Layout Two</a></li>
                                                    <li><a href="promo-layout-three.php">Promo Layout Three</a></li>

                                                </ul>
                                            </li>
                                        </ul>
                                        <span class="sub-toggle"> <i class="fa fa-plus"></i> </span></li>
                                    <li class="menu-item-has-children">
                                        <a href="#">Blog</a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="blog-list.php">Blog List</a>
                                            </li>
                                            <li>
                                                <a href="blog-full.php">Blog Full</a>
                                            </li>
                                            <li>
                                                <a href="blog-sidebar-right.php">Slidebar right</a>
                                            </li>
                                            <li>
                                                <a href="blog-sidebar-left.php">Slidebar left</a>
                                            </li>
                                        </ul>
                                        <span class="sub-toggle"> <i class="fa fa-plus"></i> </span></li>
                                    <li><a href="aboutus.php">About Us</a></li>
                                    <li>
                                        <a href="contact-1.php">Contact Us</a></li>
                                </ul>
                            </div>
                        </nav>
                    </div>
                    <div class="main-search-wrapper">
                        <div class="main-search">
                        <a href="#"><i class="icofont-search-1"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div id="content" class="site-content">
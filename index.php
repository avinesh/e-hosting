<?php include 'header.php' ?>

<div class="ehost_banner" style="background-image: url(assets/images/background_banner.png)">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-md-12">
                <div class="banner_inner_wrapper">
                    <div class="banner_content">
                        <h2> Fastest Performance Web Hosting </h2>
                        <p>Dedicated Servers are ideal for Larger Businesses and
                            High Traffic Websites</p>
                        <div class="read_more_layout_one">
                            <a href="#"> Read More</a>
                        </div>
                    </div>
                    <div class="banner_image">
                        <img src="assets/images/banner_3.png" alt=""/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ehost_search_domain">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-section">
                    <h2> Choose Your Domain Today!</h2>
                    <p>From professional business to enterprise, we've got you covered!</p>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="search_domain">
                    <form role="search" method="get" class="search-form">
                        <label>
                            <span class="screen-reader-text">Search for:</span>
                            <input type="search" class="search-field" placeholder="Search…" value="" name="s">
                        </label>
                        <input type="submit" class="search-submit" value="Search">
                    </form>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="domain_price">
                    <div class="domain_list"><span class="tld_name">.com</span> <span class="tld_price">$12</span></div>
                    <div class="domain_list"><span class="tld_name">.net</span> <span class="tld_price">$12</span></div>
                    <div class="domain_list"><span class="tld_name">.info</span> <span class="tld_price">$12</span>
                    </div>
                    <div class="domain_list"><span class="tld_name">.org</span> <span class="tld_price">$12</span></div>
                    <div class="domain_list"><span class="tld_name">.co.uk</span> <span class="tld_price">$12</span>
                    </div>
                    <div class="domain_list"><span class="tld_name">.city</span> <span class="tld_price">$12</span>
                    </div>
                    <div class="domain_list"><span class="tld_name">.edu</span> <span class="tld_price">$12</span></div>
                    </ul>
                    <div class="domain_list"><span class="tld_name">.biz</span> <span class="tld_price">$12</span></div>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="ehost_hosting_plan">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-section">
                    <h2>Our Hosting Plans</h2>
                    <p>From professional business to enterprise, we've got you covered!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="wpb_wrapper"><h3>PERSONAL UNLIMITED</h3>
                    <div class="wpb_text_column wpb_content_element  text-center plans_content_box no_margin font_weight500">

                        <div class="home_pricing_box">
                            <p class="pricing_cont color_ff007e font_size16 line_height16 text_uppercase font_weight400">
                                <span class="font_size48 line_height48 font_weight700">$49</span>/MO</p>
                            <div class="yearly_benefits_box">
                                <p>Save Up to 30% <span class="billing_tenour">Annual Billing </span></p>
                            </div>
                        </div>
                        <ul>
                            <li>Unlimited Disk Space</li>
                            <li>99.9% Uptime Guarantee</li>
                            <li>Unmetered Bandwidth</li>
                            <li>One Click Installs Scripts</li>
                        </ul>
                    </div>
                    <div class="read_more_layout_one">
                        <a href="#"> Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="wpb_wrapper active"><h3>ADVANCED UNLIMITED</h3>
                    <div class="wpb_text_column wpb_content_element  text-center plans_content_box no_margin font_weight500">

                        <div class="home_pricing_box">
                            <p class="pricing_cont color_ff007e font_size16 line_height16 text_uppercase font_weight400">
                                <span class="font_size48 line_height48 font_weight700">$49</span>/MO</p>
                            <div class="yearly_benefits_box">
                                <p>Save Up to 30% <span class="billing_tenour">Annual Billing </span></p>
                            </div>
                        </div>
                        <ul>
                            <li>Unlimited Disk Space</li>
                            <li>99.9% Uptime Guarantee</li>
                            <li>Unmetered Bandwidth</li>
                            <li>One Click Installs Scripts</li>
                        </ul>
                    </div>
                    <div class="read_more_layout_one">
                        <a href="#"> Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="wpb_wrapper"><h3>BUSINESS UNLIMITED</h3>
                    <div class="wpb_text_column wpb_content_element  text-center plans_content_box no_margin font_weight500">

                        <div class="home_pricing_box">
                            <p class="pricing_cont color_ff007e font_size16 line_height16 text_uppercase font_weight400">
                                <span class="font_size48 line_height48 font_weight700">$49</span>/MO</p>
                            <div class="yearly_benefits_box">
                                <p>Save Up to 30% <span class="billing_tenour">Annual Billing </span></p>
                            </div>
                        </div>
                        <ul>
                            <li>Unlimited Disk Space</li>
                            <li>99.9% Uptime Guarantee</li>
                            <li>Unmetered Bandwidth</li>
                            <li>One Click Installs Scripts</li>
                        </ul>
                    </div>
                    <div class="read_more_layout_one">
                        <a href="#"> Read More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="ehost_feature_services">
    <div class="container">
        <div class="row no-gutters">
            <div class="blog-post-list">
                <div class="list-post-content">
                    <h2 class="blog-title"><a href="#"> <span>Secure-Fast-Reliable</span> VPS & DEDICATED WEB
                            HOSTING</a></h2>
                    <div class="post_content">
                        <p>We provide cheapest Linux VPS Hosting in Nepal with cPanel/WHM included. With VPS hosting you
                            get root access so that you get freedom do deploy your project yourself. Take the advantage
                            of our cheap VPS hosting plan with SSD Disk space for even faster performance.</p>
                    </div>
                    <div class="read_more_layout_one">
                        <a href="#"> Read More</a>
                    </div>

                </div>
                <div class="list-post-media">
                    <a href="#">
                        <div class="image_bg" style="background-image: url(assets/images/feature_service_1.png) "></div>
                    </a>
                </div>

            </div>
            <div class="blog-post-list">
                <div class="list-post-media">
                    <a href="#">
                        <div class="image_bg" style="background-image: url(assets/images/feature_service_3.png) "></div>
                    </a>
                </div>
                <div class="list-post-content">
                    <h2 class="blog-title"><a href="#"> <span>Get into The Cloud</span>SSD CLOUD WEB HOSTING</a></h2>
                    <div class="post_content">
                        <p>We provide cheapest Linux VPS Hosting in Nepal with cPanel/WHM included. With VPS hosting you
                            get root access so that you get freedom do deploy your project yourself. Take the advantage
                            of our cheap VPS hosting plan with SSD Disk space for even faster performance.</p>
                    </div>
                    <div class="read_more_layout_one">
                        <a href="#"> Read More</a>
                    </div>
                </div>

            </div>
            <div class="blog-post-list">

                <div class="list-post-content">
                    <h2 class="blog-title"><a href="#"> <span>Start your own Business</span> RESELLER WEB HOSTING (WHM)</a>
                    </h2>
                    <div class="post_content">
                        <p>We provide cheapest Linux VPS Hosting in Nepal with cPanel/WHM included. With VPS hosting you
                            get root access so that you get freedom do deploy your project yourself. Take the advantage
                            of our cheap VPS hosting plan with SSD Disk space for even faster performance.</p>
                    </div>
                    <div class="read_more_layout_one">
                        <a href="#"> Read More</a>
                    </div>
                </div>
                <div class="list-post-media">
                    <a href="#">
                        <div class="image_bg" style="background-image: url(assets/images/feature_service_2.png) "></div>
                    </a>
                </div>
            </div>


        </div>
    </div>
</div>

<div class="e_host_why">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-section">
                    <h2> Why choose ehosting server !</h2>
                    <p>From professional business to enterprise, we've got you covered!</p>
                </div>

            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="why_ehosting_wrapper left_side">
                    <div class="why_content">
                        <h3><a href="#">Autoupdates</a></h3>
                        <p>We provide cheapest Linux VPS Hosting in Nepal </p>
                    </div>
                    <div class="icon_box"><i class="icofont-rocket"></i></div>
                </div>
                <div class="why_ehosting_wrapper left_side">
                    <div class="why_content">
                        <h3><a href="#">Server Level Protection</a></h3>
                        <p>We provide cheapest Linux VPS Hosting in Nepal </p>
                    </div>
                    <div class="icon_box"><i class="icofont-server"></i></div>
                </div>


                <div class="why_ehosting_wrapper left_side">
                    <div class="why_content">
                        <h3><a href="#">Unlimited Email Addresses</a></h3>
                        <p>We provide cheapest Linux VPS Hosting in Nepal </p>
                    </div>
                    <div class="icon_box"><i class="icofont-email"></i></div>
                </div>
                <div class="why_ehosting_wrapper left_side">
                    <div class="why_content">
                        <h3><a href="#">Free 24×7/365 Support</a></h3>
                        <p>We provide cheapest Linux VPS Hosting in Nepal </p>
                    </div>
                    <div class="icon_box"><i class="icofont-live-support"></i></div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <img src="assets/images/server_1.png" alt=""/>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="why_ehosting_wrapper right_side">
                    <div class="icon_box"><i class="icofont-download-alt"></i></div>
                    <div class="why_content">
                        <h3><a href="#">Daily Backups</a></h3>
                        <p>We keep up to 30 daily backup copies of your WordPress site.</p>
                    </div>
                </div>
                <div class="why_ehosting_wrapper right_side">
                    <div class="icon_box"><i class="icofont-finger-print"></i></div>
                    <div class="why_content">
                        <h3><a href="#">Account Isolation</a></h3>
                        <p>We provide cheapest Linux VPS Hosting in Nepal </p>
                    </div>
                </div>
                <div class="why_ehosting_wrapper right_side">
                    <div class="icon_box"><i class="icofont-clouds"></i></div>
                    <div class="why_content">
                        <h3><a href="#">CloudFlare Integration</a></h3>
                        <p>We provide cheapest Linux VPS Hosting in Nepal </p>
                    </div>
                </div>
                <div class="why_ehosting_wrapper right_side">
                    <div class="icon_box"><i class="icofont-dollar"></i></div>
                    <div class="why_content">
                        <h3><a href="#">7 Day Money-back Guarantee</a></h3>
                        <p>We provide cheapest Linux VPS Hosting in Nepal </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="e_host_testimonial">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-section">
                    <h2> What Our Customer Say</h2>
                    <p>From professional business to enterprise, we've got you covered!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="owl-carousel owl-theme testimonial-slider">
                    <div class="item">
                        <div class="testimonial_wrapper">
                            <div class="testimonial_wrapper_inner">
                                <div class="testimonial_avatar">
                                    <div class="testimonial_avatar_inner">
                                        <img src="assets/images/f3.jpg" alt=""/>
                                    </div>

                                </div>
                                <div class="testimonial_content">
                                    <p>From professional business to enterprise, we've got you covered.From professional
                                        business to enterprise, we've got you covered.</p>
                                    <h3>Prakash Mahat</h3>
                                    <span> We Design</span>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial_wrapper">
                            <div class="testimonial_wrapper_inner">
                                <div class="testimonial_avatar">
                                    <div class="testimonial_avatar_inner">
                                        <img src="assets/images/f3.jpg" alt=""/>
                                    </div>

                                </div>
                                <div class="testimonial_content">
                                    <p>From professional business to enterprise, we've got you covered.From professional
                                        business to enterprise, we've got you covered.</p>
                                    <h3>Prakash Mahat</h3>
                                    <span> We Design</span>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial_wrapper">
                            <div class="testimonial_wrapper_inner">
                                <div class="testimonial_avatar">
                                    <div class="testimonial_avatar_inner">
                                        <img src="assets/images/f3.jpg" alt=""/>
                                    </div>

                                </div>
                                <div class="testimonial_content">
                                    <p>From professional business to enterprise, we've got you covered.From professional
                                        business to enterprise, we've got you covered.</p>
                                    <h3>Prakash Mahat</h3>
                                    <span> We Design</span>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial_wrapper">
                            <div class="testimonial_wrapper_inner">
                                <div class="testimonial_avatar">
                                    <div class="testimonial_avatar_inner">
                                        <img src="assets/images/f3.jpg" alt=""/>
                                    </div>

                                </div>
                                <div class="testimonial_content">
                                    <p>From professional business to enterprise, we've got you covered.From professional
                                        business to enterprise, we've got you covered.</p>
                                    <h3>Prakash Mahat</h3>
                                    <span> We Design</span>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="testimonial_wrapper">
                            <div class="testimonial_wrapper_inner">
                                <div class="testimonial_avatar">
                                    <div class="testimonial_avatar_inner">
                                        <img src="assets/images/f3.jpg" alt=""/>
                                    </div>

                                </div>
                                <div class="testimonial_content">
                                    <p>From professional business to enterprise, we've got you covered.From professional
                                        business to enterprise, we've got you covered.</p>
                                    <h3>Prakash Mahat</h3>
                                    <span> We Design</span>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="e_host_client_logo">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-section">
                    <h2> What Our Customer Say</h2>
                    <p>From professional business to enterprise, we've got you covered!</p>
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-lg-2">
                <div class="client_logo_wrapper">
                    <div class="client_logo_inner">
                         <img src="assets/images/client_logo_1.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client_logo_wrapper">
                    <div class="client_logo_inner">
                        <img src="assets/images/client_logo_2.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client_logo_wrapper">
                    <div class="client_logo_inner">
                        <img src="assets/images/client_logo_3.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client_logo_wrapper">
                    <div class="client_logo_inner">
                        <img src="assets/images/client_logo_4.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client_logo_wrapper">
                    <div class="client_logo_inner">
                        <img src="assets/images/client_logo_5.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="client_logo_wrapper">
                    <div class="client_logo_inner">
                        <img src="assets/images/client_logo_6.png" alt="" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'footer.php' ?>

</div>


<footer id="colophon" class="site-footer">
    <div class="upper_footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <aside id="custom_html-3" class="widget_text widget widget_custom_contact"><h2 class="widget-title">
                            Custom Links</h2>
                        <div class="textwidget custom-html-widget">
                            <div class="address">

                                <img width="238" src="assets/images/footer_bg.png" alt="">
                                <br>
                                <br>
                                Balkumari -9, Ring Road,
                                Lalitpur
                                <div class="clearfix margin_bottom1"></div>
                                <strong>Phone:</strong>
                                <b>+977 985-123 -6800 </b>
                                <br>
                                <strong>Mail:</strong>
                                <a href="mailto:contact@ehostingserver.com">
                                    contact@ehostingserver.com
                                </a>
                                <br>
                                <a href="https://goo.gl/SWaIH7" class="smbut">
                                    View Directions
                                </a>
                                <br><br>
                                <img width="224" src="https://www.ehostingserver.com/wp-content/themes/arkahost/assets/images/payment-logos.png" alt="">

                            </div>
                        </div>
                    </aside>

                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <aside id="custom_html-3" class="widget_text widget widget_custom_html"><h2 class="widget-title">
                            Custom Links</h2>
                        <div class="textwidget custom-html-widget">
                            <ul class="foolist">
                                <li><a href="https://www.ehostingserver.com/linux-web-hosting-in-nepal/">Cheapest Web
                                        Hosting</a></li>
                                <li><a href="http://www.ehostingserver.com/cloud-hosting-nepal/">Cloud Hosting </a></li>
                                <li><a href="https://www.ehostingserver.com/news-portal-web-hosting/">News Portal
                                        Hosting</a></li>
                                <li><a href="https://www.ehostingserver.com/seo-optimized-hosting/">SEO Web Hosting</a>
                                </li>
                                <li><a href="http://www.ehostingserver.com/reseller-hosting/">Reseller Hosting</a></li>
                                <li><a href="https://www.ehostingserver.com/linux-vps-hosting/">Linux VPS Hosting</a>
                                </li>
                                <li><a href="https://www.ehostingserver.com/windows-cloud-vps/">Windows VPS (RDP)</a>
                                </li>
                                <li><a href="#">Dedicated Server</a></li>

                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <aside id="custom_html-3" class="widget_text widget widget_custom_html"><h2 class="widget-title">
                            Custom Links</h2>
                        <div class="textwidget custom-html-widget">
                            <ul class="foolist">
                                <li><a href="https://www.ehostingserver.com/linux-web-hosting-in-nepal/">Cheapest Web
                                        Hosting</a></li>
                                <li><a href="http://www.ehostingserver.com/cloud-hosting-nepal/">Cloud Hosting </a></li>
                                <li><a href="https://www.ehostingserver.com/news-portal-web-hosting/">News Portal
                                        Hosting</a></li>
                                <li><a href="https://www.ehostingserver.com/seo-optimized-hosting/">SEO Web Hosting</a>
                                </li>
                                <li><a href="http://www.ehostingserver.com/reseller-hosting/">Reseller Hosting</a></li>
                                <li><a href="https://www.ehostingserver.com/linux-vps-hosting/">Linux VPS Hosting</a>
                                </li>
                                <li><a href="https://www.ehostingserver.com/windows-cloud-vps/">Windows VPS (RDP)</a>
                                </li>
                                <li><a href="#">Dedicated Server</a></li>

                            </ul>
                        </div>
                    </aside>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <aside id="custom_html-3" class="widget_text widget widget_custom_html"><h2 class="widget-title">
                            Custom Links</h2>
                        <div class="textwidget custom-html-widget">
                            <ul class="foolist">
                                <li><a href="https://www.ehostingserver.com/linux-web-hosting-in-nepal/">Cheapest Web
                                        Hosting</a></li>
                                <li><a href="http://www.ehostingserver.com/cloud-hosting-nepal/">Cloud Hosting </a></li>
                                <li><a href="https://www.ehostingserver.com/news-portal-web-hosting/">News Portal
                                        Hosting</a></li>
                                <li><a href="https://www.ehostingserver.com/seo-optimized-hosting/">SEO Web Hosting</a>
                                </li>
                                <li><a href="http://www.ehostingserver.com/reseller-hosting/">Reseller Hosting</a></li>
                                <li><a href="https://www.ehostingserver.com/linux-vps-hosting/">Linux VPS Hosting</a>
                                </li>
                                <li><a href="https://www.ehostingserver.com/windows-cloud-vps/">Windows VPS (RDP)</a>
                                </li>
                                <li><a href="#">Dedicated Server</a></li>

                            </ul>
                        </div>
                    </aside>
                </div>
            </div>
        </div>
    </div>
    <div class="lower_bottom">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 lower_left">
                    <div class="copyright">© 2019 All rights reserved <a href="#">Sparkle Themes</a></div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 lower_right">
                    <ul>
                        <li><a href="#" title="Facebook"><i class="fab fa-facebook-f"></i> </a></li>
                        <li><a href="#" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                        <li><a href="#" title="Youtube"><i class="fab fa-youtube"></i> </a></li>
                        <li><a href="#" title="Tumblr"><i class="fab fa-tumblr"></i> </a></li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</footer>
</div>


<script src="assets/js/jquery.min.js" type="text/javascript"></script>
<script src="assets/library/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/library/owlcarousel/js/owl.carousel.min.js"></script>
<script src="assets/js/main.js"></script>

</body>
</html>